﻿using System;
using System.Threading.Tasks;

namespace Async_Await
{
    /// <summary>
    /// Console application class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Application entrance.
        /// </summary>
        /// <param name="args">Argumnets given to program.</param>
        /// <returns>Task to run async.</returns>
        public static async Task Main(string[] args)
        {
            var random = new Random();

            var array = await ArrayOperations.CreateTenRandomInts();

            array = await ArrayOperations.MultiplyOnRandom(array, random.Next(199));
            array = await ArrayOperations.SortAsc(array);
            double res = await ArrayOperations.Average(array);

            Console.WriteLine("\nProgram ended, finla result is {0}", res);
        }
    }
}
