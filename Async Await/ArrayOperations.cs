﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Async_Await
{
    /// <summary>
    /// Static class with array operations.
    /// </summary>
    public static class ArrayOperations
    {
        /// <summary>
        /// Creates array of 10 random integers.
        /// </summary>
        /// <returns>Array of 10 integers.</returns>
        public static async Task<int[]> CreateTenRandomInts()
        {
            int[] ints = new int[10];
            var random = new Random();

            for (int i = 0; i < ints.Length; i++)
            {
                ints[i] = random.Next(0, 300);
                Console.WriteLine("Generated number {0} with id {1}", ints[i], i);
            }

            return await Task.FromResult(ints);
        }

        /// <summary>
        /// Multiplies every element of 10-element array by given number.
        /// </summary>
        /// <param name="ints">10-element array to be multiplied.</param>
        /// <param name="randomNumber">Number to be multiplied on.</param>
        /// <returns>10-elemnt array of integers.</returns>
        /// <exception cref="ArgumentNullException">Thrown if array is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if array length is not 10.</exception>
        public static async Task<int[]> MultiplyOnRandom(int[] ints, int randomNumber)
        {
            int[] newInts = ints ?? throw new ArgumentNullException(nameof(ints));

            if (ints.Length != 10)
            {
                throw new ArgumentOutOfRangeException(nameof(ints), "Array length must equal 10.");
            }

            Console.WriteLine("Random number is {0}", randomNumber);

            for (int i = 0; i < newInts.Length; i++)
            {
                newInts[i] *= randomNumber;
                Console.WriteLine("New {0} is {1}", i, newInts[i]);
            }

            return await Task.FromResult(newInts);
        }

        /// <summary>
        /// Sorts array ascending.
        /// </summary>
        /// <param name="ints">10-element array to be sorted.</param>
        /// <returns>10-element array of ints.</returns>
        /// <exception cref="ArgumentNullException">Thrown if array is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if array length is not 10.</exception>
        public static async Task<int[]> SortAsc(int[] ints)
        {
            int[] newInts = ints ?? throw new ArgumentNullException(nameof(ints));

            if (ints.Length != 10)
            {
                throw new ArgumentOutOfRangeException(nameof(ints), "Array length must equal 10.");
            }

            Array.Sort(newInts);

            for (int i = 0; i < newInts.Length; i++)
            {
                Console.WriteLine("Sorted value {0} is {1}", i, newInts[i]);
            }

            return await Task.FromResult(newInts);
        }

        /// <summary>
        /// Gets average value of 10-element array.
        /// </summary>
        /// <param name="ints">10element array of ints to be counted.</param>
        /// <returns>Average value of given array.</returns>
        /// <exception cref="ArgumentNullException">Thrown if array is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if array length is not 10.</exception>
        public static async Task<double> Average(int[] ints)
        {
            if (ints == null)
            {
                throw new ArgumentNullException(nameof(ints));
            }

            if (ints.Length != 10)
            {
                throw new ArgumentOutOfRangeException(nameof(ints), "Array length must equal 10.");
            }

            var average = ints.ToList().Average();
            Console.WriteLine("Average value of array is {0}", average);

            return await Task.FromResult(average);
        }
    }
}
