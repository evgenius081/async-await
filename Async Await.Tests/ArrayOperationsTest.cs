using System;
using System.IO;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Async_Await.Tests
{
    /// <summary>
    /// Tests for <see cref="ArrayOperations"> class.
    /// </summary>
    public class ArrayOperationsTest
    {
        /// <returns>Task because it is async method.</returns>
        /// <summary>
        /// Tests correct behaviour of <see cref="ArrayOperations.CreateTenRandomInts"> method.
        /// </summary>
        [Test]
        public async Task CreateTenRandomIntsCorrect()
        {
            // Act
            int[] res = await ArrayOperations.CreateTenRandomInts();

            // Assert
            Assert.IsTrue(res.Length == 10);
        }

        /// <returns>Task because it is async method.</returns>
        /// <summary>
        /// Tests correct behaviour of <see cref="ArrayOperations.MultiplyOnRandom(int[], int)"> method.
        /// </summary>
        /// <param name="ints">Array of integers.</param>
        /// <param name="multiplier">Multiplier value.</param>
        /// <param name="expected">Expected value.</param>
        [TestCase(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, 2, new int[] { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 })]
        public async Task MultiplyOnRandomIntCorrect(int[] ints, int multiplier, int[] expected)
        {
            // Arrange
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);
            var expectedConsoleOutput = "Random number is 2\r\nNew 0 is 2\r\nNew 1 is 4\r\nNew 2 is 6\r\nNew 3 is 8\r\nNew 4 is 10\r\nNew 5 is 12\r\nNew 6 is 14\r\nNew 7 is 16\r\nNew 8 is 18\r\nNew 9 is 20\r\n";

            // Act
            int[] actual = await ArrayOperations.MultiplyOnRandom(ints, multiplier);

            // Assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedConsoleOutput, stringWriter.ToString());
        }

        /// <returns>Task because it is async method.</returns>
        /// <summary>
        /// Tests correct behavoiur of <see cref="ArrayOperations.SortAsc(int[])"> method.
        /// </summary>
        /// <param name="ints">Array of integers.</param>
        /// <param name="expected">Expected value.</param>
        [TestCase(new int[] { 10, 2, 3, 8, 5, 7, 6, 4, 9, 1 }, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 })]
        public async Task SortAscCorrect(int[] ints, int[] expected)
        {
            // Arrange
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);
            var expectedConsoleOutput = "Sorted value 0 is 1\r\nSorted value 1 is 2\r\nSorted value 2 is 3\r\nSorted value 3 is 4\r\nSorted value 4 is 5\r\nSorted value 5 is 6\r\nSorted value 6 is 7\r\nSorted value 7 is 8\r\nSorted value 8 is 9\r\nSorted value 9 is 10\r\n";

            // Act
            int[] actual = await ArrayOperations.SortAsc(ints);

            // Assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedConsoleOutput, stringWriter.ToString());
        }

        /// <returns>Task because it is async method.</returns>
        /// <summary>
        /// Tests correct behaviour of <see cref="ArrayOperations.Average(int[])"> method.
        /// </summary>
        /// <param name="ints">Array of integers.</param>
        /// <param name="expected">Expected value.</param>
        [TestCase(new int[] { 10, 2, 3, 8, 5, 7, 6, 4, 9, 1 }, 5.5)]
        public async Task AverageCorrect(int[] ints, double expected)
        {
            // Arrange
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);
            var expectedConsoleOutput = "Average value of array is 5,5\r\n";

            // Act
            double actual = await ArrayOperations.Average(ints);

            // Assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedConsoleOutput, stringWriter.ToString());
        }

        /// <summary>
        /// Tests behaviour of <see cref="ArrayOperations.MultiplyOnRandom(int[], int)"> method in case array is null.
        /// </summary>
        /// <param name="ints">Array of integers.</param>
        /// <param name="multiplier">Multiplier value.</param>
        [TestCase(null, 2)]
        public void MultiplyOnRandomIntNullException(int[] ints, int multiplier)
        {
            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => ArrayOperations.MultiplyOnRandom(ints, multiplier));
        }

        /// <summary>
        /// Tests behaviour of <see cref="ArrayOperations.SortAsc(int[])"> method in case array is null.
        /// </summary>
        /// <param name="ints">Array of integers.</param>
        [TestCase(null)]
        public void SortAscNullException(int[] ints)
        {
            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => ArrayOperations.SortAsc(ints));
        }

        /// <summary>
        /// Tests behaviour of <see cref="ArrayOperations.Average(int[])"> method in case array is null.
        /// </summary>
        /// <param name="ints">Array of integers.</param>
        [TestCase(null)]
        public void AverageNullException(int[] ints)
        {
            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => ArrayOperations.Average(ints));
        }

        /// <summary>
        /// Tests behaviour of <see cref="ArrayOperations.MultiplyOnRandom(int[], int)"> method in case array length is not 10.
        /// </summary>
        /// <param name="ints">Array of integers.</param>
        /// <param name="multiplier">Multiplier value.</param>
        [TestCase(new int[] { }, 2)]
        [TestCase(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, 2)]
        [TestCase(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, 2)]
        public void MultiplyOnRandomIntOutOfRangeException(int[] ints, int multiplier)
        {
            // Assert
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => ArrayOperations.MultiplyOnRandom(ints, multiplier));
        }

        /// <summary>
        /// Tests behaviour of <see cref="ArrayOperations.SortAsc(int[])"> method in case array length is not 10.
        /// </summary>
        /// <param name="ints">Array of integers.</param>
        [TestCase(new int[] { })]
        [TestCase(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 })]
        [TestCase(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 })]
        public void SortAscOutOfRangeException(int[] ints)
        {
            // Assert
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => ArrayOperations.SortAsc(ints));
        }

        /// <summary>
        /// Tests behaviour of <see cref="ArrayOperations.Average(int[])"> method in case array length is not 10.
        /// </summary>
        /// <param name="ints">Array of integers.</param>
        [TestCase(new int[] { })]
        [TestCase(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 })]
        [TestCase(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 })]
        public void AveragOutOfRangeException(int[] ints)
        {
            // Assert
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => ArrayOperations.Average(ints));
        }
    }
}